# version 1.1

# Import des librairies dont nous aurons besoin
from random import randint, choice # Cela va nous permettre d'intégrer l'aléatoire dont nous aurons besoin à plusieurs endroits
import matplotlib.pyplot as plt # On aura besoin de cette librairie notamment pour tracer des graphes
from Classes import Monde # On importe la classe Monde du fichier Classes.py
from Classes import Mouton # On importe la classe Mouton du fichier Classes.py
from Classes import Simulation # On importe la classe Simulation du fichier Classes.py


# Execution de la simulation

# On crée le "dico_mouton" qui contient toutes les informations sur les moutons :
nombre_de_moutons = 50 # Le nombre de moutons voulus initialement sur la carte
gain_de_nourriture = 20 # Le gain de nourriture voulu
duree_repousse_laine = 5 # Le temps que met la lainu mouton a avoir complètement poussé
taux_de_reproduction = 10 # Le taux de reproduction des moutons
nombre_max_moutons = 50000000 # Un nombre maximal de moutons qui arrêterai la simulation une fois atteint
dico_mouton = {
    "nb" : nombre_de_moutons, 
    "gain_n" : gain_de_nourriture, 
    "duree_laine" : duree_repousse_laine, 
    "taux_rep" : taux_de_reproduction, 
    "moutons_max" : nombre_max_moutons
    }

# On crée le "dico_loup" qui contient toutes les informations sur les loups4
nombre_de_loups = 20 # Le nombre de loups  voulus initalement sur la carte
gain_de_nourriture = 15 # Le gain de nourriture voulu
taux_de_reproduction = 10 # Le taux de reproduction des loups
proba_manger = 25 # La probabilité pour un loup de réussir à manger un mouton
dico_loup = {
    "nb" : nombre_de_loups, 
    "gain_n" : gain_de_nourriture,  
    "taux_rep" : taux_de_reproduction,
    "proba_manger" : proba_manger
    }

# On crée le "dico_chasseur" qui contient toutes les informations sur les chasseurs
nombre_de_chasseurs = 5 # Le nombre de chasseurs voulus sur la carte
taux_de_precision = 30 # Le taux de précision à chaque tir du chasseur
dico_chasseur = {
    "nb" : nombre_de_chasseurs,
    "taux_prec" : taux_de_precision
    }

# On crée le monde qui va être habité
monde = Monde(
    dimension = 50, 
    duree_repousse = 30
    ) # On crée un monde

# Et on crée l'objet simulation qui va être utilisé
simulation = Simulation(
    fin = 100,
    monde = monde,
    dico_mouton = dico_mouton,
    dico_loup = dico_loup,
    dico_chasseur = dico_chasseur, 
    nb_tondeurs = 5
)

# On éxécute le tout et on récupère les résultats sous formes de listes
liste_herbe, liste_moutons, liste_loups, score_chasseurs, capital_tondeurs = simulation.sim()

# On cherche le chasseur gagant :
cle_maxi_c = max(score_chasseurs, key = score_chasseurs.get) # On cherche le nom du chasseur gagnant
maxi_c = score_chasseurs[cle_maxi_c] # On cherche le nombre de victimes du gagnant

cle_maxi_t = max(capital_tondeurs, key = capital_tondeurs.get) # On fait de même pour les tondeurs
maxi_t = capital_tondeurs[cle_maxi_t]

score = 0
for i in score_chasseurs.keys(): # On fait une somme de tous les scores
    score += score_chasseurs[i]

capital = 0
for i in capital_tondeurs.keys():
    capital += capital_tondeurs[i]


print(
    "Nombre d'herbes à la fin de la simulation : " + str(liste_herbe[-1])
    + "\nNombre de moutons à la fin de la simulation : " + str(liste_moutons[-1]) 
    + "\nNombre de loups à la fin de la simulation : " + str(liste_loups[-1])
    + "\nLe chasseur ayant tué le plus de loups est " + str(cle_maxi_c) + ", " + str(maxi_c) + " loup(s) sont morts sous ses balles."
    + "\nScore total des chasseurs à la fin de la simulation : " + str(score)
    + "\nLe tondeur ayant le plus grand capital est " + str(cle_maxi_t) + ", " + str(maxi_t) + " mouton(s) ont été victime(s) de ses cisailles."
    + "\nCapital total des tondeurs à la fin de la simulation : " + str(capital)
    )

# On trace les graphes
plt.grid(True)
plt.plot([i for i in range(len(liste_herbe))], liste_herbe, "g", linewidth = 0.8, label = "Herbe")
plt.plot([i for i in range(len(liste_herbe))], liste_moutons, "b", linewidth = 0.8, label = "Moutons")
plt.plot([i for i in range(len(liste_herbe))], liste_loups, "r", linewidth = 0.8, label = "Loups")
plt.xlabel('Temps')
plt.ylabel('Nombre')
plt.legend()
plt.show()
