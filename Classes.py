# version 1.1

# Import des librairies nécessaires
from random import randint, random, choice # Cela va nous permettre d'intégrer l'aléatoire dont nous aurons besoin à plusieurs endroits

class Monde :
    # Cette classe représente le monde dans lequel évolue l'écosystème, plus précisément sa carte, sous forme d'un matrice carrée de côté x

    def __init__(self, dimension, duree_repousse): 
        '''
        On définit l'objet Monde
        Prec : 
        - Une dimension (int), qui sera la longueur x du côté de la matrice carrée, 
        - et une duree de repousse (int) qui définira la durée de repousse de l'hebre
        Postc : L'objet monde
        '''
        self.__dimension = dimension # On récupère la dimension qu'aura la carte
        self.__carte = [
            [ # On crée une matrice carrée d'entiers de côté x, chaque élément est un dictionnaire qui contient de l'herbre et les "êtres" présentes sur cette case
                {
                    "herbe" : 0, 
                    "mouton" : [], 
                    "loup" : [], 
                    "chasseur" : [], 
                    "tondeur" : []
                } for i in range (dimension)] for i in range (dimension) 
            ] 
        for i in self.__carte: # On rempli une case sur deux avec de l'herbe, sur la case vide, on met un debout de repousse
            for j in i:
                if randint(1,2) == 1:
                    j["herbe"] = duree_repousse
                else:
                    j["herbe"] = randint(1, duree_repousse - 1)
        self.__herbe_repousse = duree_repousse # On définit la durée de repousse de l'herbe

    def __repr__(self):
        '''
        On renvoie la liste de listes qui représente la matrice carrée qui représente elle-même le monde
        Postc : La carte du monde (de type Monde) exprimée sous forme de liste (list) de listes (list)
        '''
        return str(self.__carte)

    def get_infos(self):
        '''
        Cette fonction va nous permette de récupérer les attributs de l'objet
        Prec : Demande des attributs
        Post : Un dictionnaire (dict) avec les attributs
        '''
        return dict(
            {
                "dimension" : self.__dimension, 
                "repousse" : self.__herbe_repousse
            }
            )

    def herbePousse(self):
        '''
        On fait pousser l'herbe dans chaque case de la matrice
        Postc : La matrice monde (Monde) avec chaque case qui a gagné un niveau de repousse d'herbe
        '''
        for i in range (len(self.__carte)): # On parcours les listes de listes
            for j in range (len(self.__carte)): # On parcours les sous-listes
                self.__carte[i][j]["herbe"] += 1 # On incrémente une valeur d'herbe

    def herbeMangee(self, x, y):
        '''
        On retire l'herbe d'une case de la matrice
        Prec : Un x (int) et un y (int) qui inidiquent une case à réitnitalisée
        Post : La matrice (Monde) avec la case réinitialisée
        '''
        self.__carte[x][y]["herbe"] = 0
 
    def getCoefCarte(self, x, y):
        '''
        On cherhce l'entier (int) qui se trouve dans la case d'indice donnée en paramètre
        Prec : Un x (int) et un y (int) servants d'indices pour trouver la case
        Postc : L'entier qui se trouve dans la case demandée-*
        '''
        return dict(self.__carte[x][y])

    def placer_etre(self, etre):
        '''
        Cette méthode va servir à placer un être sur la matrice du monde
        Prec : L'être qu'il faut placer (sa classe peut varier)
        Postc : L'être placé sur la carte
        '''
        if type(etre) == Mouton:
            info = etre.get_infos()
            self.__carte[info["position"][0]][info["position"][1]]["mouton"].append(etre)
        elif type(etre) == Loup:
            info = etre.get_infos()
            self.__carte[info["position"][0]][info["position"][1]]["loup"].append(etre)
        elif type(etre) == Chasseur:
            info = etre.get_infos()
            self.__carte[info["position"][0]][info["position"][1]]["chasseur"].append(etre) 
        
    def retirer_etre(self, etre):
        '''
        Cette méthode va servir à retirer un être de la carte, soit parce qu'il est mort soit pour mettre à jour sa position après un déplacement
        Prec : L'être est présent sur la carte (sa classe peut varier)
        Postc : L'être a été retiré de la carte
        '''
        if type(etre) == Mouton:
            info = etre.get_infos()
            self.__carte[info["position"][0]][info["position"][1]]["mouton"].remove(etre)
        elif type(etre) == Loup:
            info = etre.get_infos()
            self.__carte[info["position"][0]][info["position"][1]]["loup"].remove(etre)
        elif type(etre) == Chasseur:
            info = etre.get_infos()
            self.__carte[info["position"][0]][info["position"][1]]["chasseur"].remove(etre) 

    def compte(self):
        '''
        Cette méthode va nous servir à dénombrer les différents éléments du mondes pour créer les graphes
        Postc : Un dictionnaire avec le nombre d'occurences de chaque élément sur la carte
        '''
        dico = {
            "herbe" : 0, 
            "mouton" : 0, 
            "loup" : 0
            }
        for i in self.__carte:
            for j in i:
                if j["herbe"] >= self.__herbe_repousse:
                    dico["herbe"] += 1
                dico["mouton"] += len(j["mouton"])
                dico["loup"] += len(j["loup"])
        return dico
    
    def plus_proche(self, x, y, etre):
        '''
        Cette méthode permet de localiser l'être le plus proche de coordonnées données
        Prec : Un être du type de sa classe
        Postc : Les coordonnées de la case la plus proche où se trouve l'être cherché
        '''
        min = [9999,9999,9999] # On va stocker dans cette liste la distance à la case donnée de la case la plus proche ainsi que ses coordonnées
        for i in range(self.get_infos()["dimension"]):
            for j in range(self.get_infos()["dimension"]):
                case = self.getCoefCarte(i,j)
                if case[etre] != []:
                    distance_x = abs(x-i)
                    if distance_x > (self.get_infos()["dimension"])//2:
                        distance_x = self.get_infos()["dimension"] - distance_x
                    distance_y = abs(y-j)
                    if distance_y > (self.get_infos()["dimension"])//2:
                        distance_y = self.get_infos()["dimension"] - distance_y
                    if distance_x + distance_y < min[0]:
                        min[0] = distance_x + distance_y
                        min[1], min[2] = i, j
        return min

# On crée une fonction qui génère des noms pour les différents individus de l'écosystème 

# On veut une liste de noms pour chaque groupe :
noms_moutons_m = {"noms" : ["Mouwton Isaac L'Originel"], "num" : 1}
noms_moutons_f = {"noms" : ["Mouwtone Esaac L'Originelle"], "num" : 1}
noms_loups_m = {"noms" : ["Loup Le Grand L'Originel"], "num" : 1}
noms_loups_f = {"noms": ["Louve La Grande L'Originelle"], "num" : 1}
noms_chasseurs_m = {"noms" : ["Chasseur Flowter L'Originel"], "num" : 1}
noms_chasseurs_f = {"noms" : ["Chasseuse Flowteuse L'Originelle"], "num" : 1}
noms_tondeurs_m = {"noms" : ["Cotoneur L'original"], "num" : 1}
noms_tondeurs_f = {"noms" : ["Cotoneuse L'originelle"], "num" : 1}

# On crée une fonction va retourner un nom en fonction d'une demande
def noms(liste,sexe):
    '''
    Cette fonction renvoie un nom pour un type d'être demandé
    Prec : Le nom de ce qu'il faut nommer (str), et le sexe de l'être
    Postc : Un nom pour l'être voulu (str)
    '''
    if liste == "mouton":
        if sexe == "M":
            noms_moutons_m["noms"].append("Mouwton Isaac " + str(noms_moutons_m["num"]))
            noms_moutons_m["num"] += 1
            return noms_moutons_m["noms"].pop(0)
        else:
            noms_moutons_f["noms"].append("Mouwtone Esaac " + str(noms_moutons_f["num"]))
            noms_moutons_f["num"] += 1
            return noms_moutons_f["noms"].pop(0)
    elif liste == "loup":
        if sexe == "M":
            noms_loups_m["noms"].append("Loup Le Grand " + str(noms_loups_m["num"]))
            noms_loups_m["num"] += 1
            return noms_loups_m["noms"].pop(0)
        else:
            noms_loups_f["noms"].append("Louve La Grande " + str(noms_loups_f["num"]))
            noms_loups_f["num"] += 1
            return noms_loups_f["noms"].pop(0)
    elif liste == "chasseur":
        if sexe == "M":
            noms_chasseurs_m["noms"].append("Chasseur Flowter " + str(noms_chasseurs_m["num"]))
            noms_chasseurs_m["num"] += 1
            return noms_chasseurs_m["noms"].pop(0)
        else:
            noms_chasseurs_f["noms"].append("Chasseur Flowter " + str(noms_chasseurs_f["num"]))
            noms_chasseurs_f["num"] += 1
            return noms_chasseurs_f["noms"].pop(0)
    elif liste == "tondeur":
        if sexe == "M":
            noms_tondeurs_m["noms"].append("Cotoneur " + str(noms_tondeurs_m["num"]))
            noms_tondeurs_m["num"] += 1
            return noms_tondeurs_m["noms"].pop(0)
        else:
            noms_tondeurs_f["noms"].append("Cotoneuse " + str(noms_tondeurs_f["num"]))
            noms_tondeurs_f["num"] += 1
            return noms_tondeurs_f["noms"].pop(0)

class Mouton :
    # Cette classe représente ce qu'est un mouton, et la façon dont il va pouvoir vivre et évoluer dans l'écosystème

    def __init__(self, gain_n, x, y, duree_laine, taux_rep, monde_du_mouton):
        '''
        On définit l'objet Mouton, il aura comme attributs :
        - un nom (chr)
        - un gain de nourriture par carré d'herbe (int), 
        - une position x et y sur le monde (int et int), 
        - une énergie qui se remplit quand il mange (int),
        - une instance du monde sur lequel il va être placé (Monde), 
        - niveau de laine que des coutiriers peuvent récupérer (int), 
        - un taux de reproduction qui définit ses chances de se reproduire en pourcentage (int)
        - et un sexe choisit aléatoirement (str)
        Prec : 
        - Un gain de nourriture, notée gain_n (int), 
        - une position x (int) et y (int), 
        - une durée de production de laine (int), 
        - et un taux de reproduction (int)
        - un monde (Monde) sur lequel on va placer le mouton
        Postc : Un mouton (Mouton) avec ses différents attributs
        '''
        self.__sexe = choice(["M", "F"]) # On donne alétoirement un sexe au mouton
        self.__nom = noms("mouton", str(self.__sexe)) # On donne un nom au mouton en fonction de son sexe
        self.__gain_nourriture = gain_n # On définit le gain de nourriture par carré d'herbe mangé
        self.__positionx = x # On donne une position x
        self.__positiony = y # On donne une position y
        self.__energie = randint(1, 2) * gain_n # On donne une valeur d'énergie initale au mouton qui représente soit un soit deux carrés d'herbe mangés
        self.__laine = randint(0,duree_laine) # On définit un attribut qui va contenir la laine du mouton
        self.__repousse_laine = duree_laine # On définit le temps que met la laine à avoir pousser et être récupérable 
        self.__taux_rep = taux_rep # On définit le taux de reproductio
        self.__monde = monde_du_mouton # On crée une instance du monde dans lequel se trouve le mouton
        monde_du_mouton.placer_etre(self) # On place le mouton sur la carte


    def __repr__(self):
        '''
        On définit une fonction __repr__ pour obtenir le nom du mouton une fois appelée
        Postc : Le nom (str) du mouton
        '''
        return str(self.__nom)

    def get_infos(self): # On n'utilise pas la fonction __repr__ pour obtenir un dictionnaire à la sortie
        '''
        La fonction va nous servir à récupérer les données du mouton
        Prec : Un mouton (Mouton)
        Post : Un dictionnaire (dict) avec toutes ses informations
        '''
        return dict(
            {
                "nom" : self.__nom, 
                "position" : [self.__positionx,self.__positiony], 
                "energie" : self.__energie, 
                "laine" : self.__laine,
                "laine_repousse" : self.__repousse_laine,
                "taux_reproduction" : self.__taux_rep,
                "sexe" : self.__sexe
            }
            )
    
    def variationEnergie(self):
        '''
        On crée la méthode qui va permettre au mouton de restaurer son énergie en mangeant de l'herbe ou d'en perdre s'il n'en mange pas 
        Prec : Un mouton (Mouton) avec une position x et y
        Post : Le mouton a reçu ou non de l'énergie (int) s'il y avait de l'herbe sur sa case, sinon il en perd, l'herbe mangée est aussi retirée de la case
        '''
        if self.__monde.get_infos()["repousse"] <= self.__monde.getCoefCarte(self.__positionx,self.__positiony)["herbe"]: # On vérifie s'il y a assez d'herbe sur la case pour qu'elle soit mangée
            self.__energie += self.__gain_nourriture # On ajoute l'énergie au mouton
            self.__monde.herbeMangee(self.__positionx,self.__positiony) # On retire l'herbe mangée de la case
        else:
            self.__energie -= 1

        return self.__energie
    
    def deplacement(self):
        '''
        On crée une fonction qui va faire se déplacer le mouton (Mouton) dans une des huits cases qui l'entourent
        Prec : Le mouton (Mouton) à sa position initale
        Post : Le mouton (Mouton) avec sa nouvelle position
        '''
        self.__monde.retirer_etre(self) # On retire le mouton de la carte pour mettre à jour sa position

        # On ajoute aléatoirement une valeur comprise entre -1 et 1 à sa position x 
        # La valeur finale obtenue est mise modulo à la taille du monde car le monde est torique :
        self.__positionx = (self.__positionx + randint(-1, 1)) % self.__monde.get_infos()["dimension"]
        # On fait de même avec la position y 
        self.__positiony = (self.__positiony + randint(-1, 1)) % self.__monde.get_infos()["dimension"] 
        
        self.__monde.placer_etre(self) # On replace le mouton avec sa nouvelle position
    
    def laine_pousse(self):
        '''
        On fait pousser la laine du mouton
        Prec : Un mouton (Mouton)
        Postc : Le mouton avec sa laine qui a poussé
        '''
        if self.__laine < self.__repousse_laine: # Si le mouton n'a pas trop de laine déjà sur lui
            self.__laine += 1

    def laine_coupee(self):
        '''
        On coupe la laine du mouton
        Prec : Un mouton (Mouton)
        Postc : Ce mouton sans la laine qu'il avait auparavant
        '''
        self.__laine = 0

class Loup :
    # Cette classe représente ce qu'est un loup, et la façon dont il va pouvoir vivre et évoluer dans l'écosystème
    
    def __init__(self, gain_n, x, y, taux_rep, proba_manger, monde_du_loup):
        '''
        On définit l'objet loup, il aura comme attributs :
        - un nom (chr)
        - un gain de nourriture par mouton mangé (int),
        - une position x et y sur le monde (int et int),
        - une énergie qui se remplit quand il mange un mouton (int),
        - une instance du monde sur lequel il va être placé (Monde),
        - un taux de reproduction qui définit ses chances de se reproduite (int),
        - une probabilité de réussir à manger un mouton qui se trouve sur la même case que lui (int),
        - et un sexe choisit aléatoirement
        Prec : 
        - Un gain de nourriture, notée gain_n (int), 
        - une position x (int) et y (int), 
        - une probabilité de manger (int), 
        - et un taux de reproduction (int)
        - un monde (Monde) sur lequel on va placer le loup
        Postc : Un loup (Loup) avec ses différents attributs

        '''
        self.__sexe = choice(["M", "F"]) # On donne aléatoirement un sexe au loup
        self.__nom = noms("loup", str(self.__sexe)) # On donne un nom au loup en fonction de son sexe
        self.__gain_nourriture = gain_n # On définit le gain de nourriture par mouton manger 
        self.__positionx = x # On donne une position x
        self.__positiony = y # On donne une position y
        self.__energie = randint(1, 2) * gain_n # On donne une valeur d'énergie initale au loup qui représente soit un soit deux moutons mangés
        self.__taux_rep = taux_rep # On définit le taux de reproduction
        self.__monde = monde_du_loup # On crée une instance du monde dans lequel se trouve le mouton
        monde_du_loup.placer_etre(self) # On place le mouton sur la carte
        self.__proba_manger = proba_manger # On définit la probabilité qu'un loup a de mange un mouton présent sur la même case que lui

    def __repr__(self):
        '''
        On définit une fonction __repr__ pour obtenir le nom du loup une fois appelée
        Postc : Le nom (str) du loup
        '''
        return str(self.__nom)

    def get_infos(self):
        '''
        La fonction va nous servir à récupérer les données du loup
        Prec : Un loup (Loup)
        Post : Un dictionnaire (dict) avec toutes ces informations
        '''
        return dict(
            {
                "nom" : self.__nom, 
                "position" : [self.__positionx, self.__positiony], 
                "energie" : self.__energie, 
                "taux_reproduction" : self.__taux_rep,
                "sexe" : self.__sexe
            }
            )

    def variationEnergie(self):
        '''
        On crée la méthode qui va permettre au loup de manger un mouton
        Prec : Un loup (Loup) avec une position x et y
        Postc : Une liste (list) avec l'énergie du loup (int), et le mouton (Mouton) qui a été tué
        '''
        if len(self.__monde.getCoefCarte(self.__positionx,self.__positiony)["mouton"]) >= 1: # On vérifie s'il y a un mouton sur la case du loup
            if randint(1,100) <= self.__proba_manger: # On teste si le loup va manger ou non le moutons sur la case
                self.__energie += self.__gain_nourriture
                return [self.__energie, choice(self.__monde.getCoefCarte(self.__positionx,self.__positiony)["mouton"])]
            else :
                self.__energie -= 1
                return [self.__energie, None]
        else: 
            self.__energie -= 1
            return [self.__energie, None]

    def deplacement(self):
        '''
        On crée la méthode qui va permettre de déplacer le loup sur la carte, 
        elle va être construite de façon à ce que si un mouton se trouve assez près du loup, 
        ce dernier va le sentir et se rapprocher de lui
        Prec : Le loup (Loup) avec sa position initale
        Postc : Le loup (Loup) avec sa position mise à jour
        '''
        self.__monde.retirer_etre(self) # On retire le loup de la carte pour mettre à jour sa position

        x, y = self.__positionx, self.__positiony # On récupère la case du loup
        d = self.__monde.plus_proche(x,y,"mouton")[0] # On récupère la distance du mouton le plus proche
        
        # Dans la méthode plus_proche(), 
        # la liste initale avait des valeurs gigantesques qui étaient remplacées par le mouton le plus proche,
        # si ces valeurs n'ont pas été remplacées, il n'y a pas de mouton sur la carte, 
        # on déplace alors juste le loup aléatoirement
        # Il y a aussi le cas où l'être cherché est trop loin, on applique alors aussi un déplacement aléatoire
        if d > 999 or self.__monde.plus_proche(x, y, "mouton")[0] > self.__monde.get_infos()["dimension"]//2 + 1: 
            # On ajoute aléatoirement une valeur comprise entre -1 et 1 à sa position x 
            # La valeur finale obtenue est mise modulo à la taille du monde car le monde est torique :
            self.__positionx = (self.__positionx + randint(-1, 1)) % self.__monde.get_infos()["dimension"]
            # On fait de même avec la position y 
            self.__positiony = (self.__positiony + randint(-1, 1)) % self.__monde.get_infos()["dimension"]
        else : # On va, par force brute, trouver la direction prise qui raproche le plus le loup du mouton
            distance_min = [self.__monde.plus_proche(x, y, "mouton")[0], 0, 0]
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    if self.__monde.plus_proche(x + i, y + j, "mouton")[0] < distance_min[0]:
                        distance_min[0] = self.__monde.plus_proche(x + i, y + j, "mouton")[0]
                        distance_min[1], distance_min[2] =  i, j
            self.__positionx = (self.__positionx + distance_min[1]) % self.__monde.get_infos()["dimension"]
            self.__positiony = (self.__positiony + distance_min[2]) % self.__monde.get_infos()["dimension"]
            
        
        self.__monde.placer_etre(self) # On replace le loup avec sa nouvelle position

class Chasseur :
    # Cette classe représente ce qu'est un chasseur, et la façon dont il va pouvoir vivre et évoluer dans l'écosystème

    def __init__(self, x, y, taux_prec, monde_du_chasseur):
        '''
        On définit l'objet chasseur, il aura comme attributs :
        - un nom (chr),
        - une position x et y sur la carte (int et int),
        - un taux de précision quand il essaie de tuer un loup (int),
        - un score, qui correspond au nombre de loups tués (int),
        - une instance du monde sur lequel il va être placé (Monde),
        - et un sexe choisi aléatoirement (chr)
        Prec :
        - Une position x et y (int et int),
        - un taux de précision (int) ,
        - et une instance du monde sur lequel il va être placé (Monde)
        Postc : Un chasseur (Chasseur) avec ses différents attributs
        '''
        self.__sexe = choice(["M", "F"]) # On donne aléatoirement un sexe au chasseur
        self.__nom = noms("chasseur", str(self.__sexe)) # On donne un nom au chasseur en fonction de son sexe
        self.__positionx = x # On donne une position x
        self.__positiony = y # On donne une position y
        self.__taux_prec = taux_prec # On donne au chasseur le taux de précision passé en 
        self.__score = 0 # C'est dans cet attribut que nous allons stocker le score du chasseur
        self.__monde = monde_du_chasseur # On crée une instance du monde dans lequel se trouve le chasseur
        monde_du_chasseur.placer_etre(self) # On place le chasseur sur la carte

    def __repr__(self):
        '''
        On définit une fonction __repr__ pour obtenir le nom du chasseur une fois appelée
        Postc : Le nom (str) du chasseur
        '''
        return str(self.__nom)
    
    def get_infos(self): 
        '''
        La fonction va nous servir à récupérer les données du chasseur
        Prec : Un chasseur (Chasseur)
        Postc : Un dictionnaire (dict) avec toutes ses informations
        '''
        return dict(
            {
                "nom" : self.__nom,
                "position" : [self.__positionx, self.__positiony],
                "score" : self.__score,
                "taux_prec" : self.__taux_prec,
                "sexe" : self.__sexe
            }
        )
    
    def chasser(self):
        '''
        On crée la méthode qui va permettre au chasseur d'attaquer un loup se trouvant sur la même case que lui
        Prec : Un chasseur (Chasseur)
        Postc : Le loup (Loup) qui a été tué ou un None s'il n'y a pas de mort
        '''
        if len(self.__monde.getCoefCarte(self.__positionx,self.__positiony)["loup"]) >= 1: # On vérifie s'il y a un loup sur la case du chasseur
            if randint(1,100) <= self.__taux_prec: # On teste si le chasseur va toucher son tir ou non
                self.__score += 1 # On incrémente le score du chasseur
                return choice(self.__monde.getCoefCarte(self.__positionx,self.__positiony)["loup"]) # On choisit aléatoirement le loup sur la case qui mourra 
            else :
                return "Raté" # On retourne ce message dans le cas où le chasseur rate son coup
        else: 
            return None

    def deplacement(self):
        '''
        On crée la méthode qui va permettre de déplacer le chasseur sur la carte, 
        elle va être construite de façon à ce que si un loup se trouve assez près du chasseur, 
        ce dernier va le sentir et se rapprocher de lui
        Prec : Le chasseur (Chasseur) avec sa position initale
        Postc : Le chasseur (Chasseur) avec sa position mise à jour
        '''
        self.__monde.retirer_etre(self) # On retire le chasseur de la carte pour mettre à jour sa position

        x, y = self.__positionx, self.__positiony # On récupère la case du chasseur
        d = self.__monde.plus_proche(x,y,"loup")[0] # On récupère la distance du loup le plus proche
        
        # Dans la méthode plus_proche(), 
        # la liste initale avait des valeurs gigantesques qui étaient remplacées par le loup le plus proche,
        # si ces valeurs n'ont pas été remplacées, il n'y a pas de loup sur la carte, 
        # on déplace alors juste le chasseur aléatoirement
        # Il y a aussi le cas où l'être cherché est trop loin, on applique alors aussi un déplacement aléatoire
        if d > 999 or self.__monde.plus_proche(x, y, "loup")[0] > self.__monde.get_infos()["dimension"]//2 + 1: 
            # On ajoute aléatoirement une valeur comprise entre -1 et 1 à sa position x 
            # La valeur finale obtenue est mise modulo à la taille du monde car le monde est torique :
            self.__positionx = (self.__positionx + randint(-1, 1)) % self.__monde.get_infos()["dimension"]
            # On fait de même avec la position y 
            self.__positiony = (self.__positiony + randint(-1, 1)) % self.__monde.get_infos()["dimension"]
        else : # On va, par force brute, trouver la direction prise qui raproche le plus le chasseur du loup
            distance_min = [self.__monde.plus_proche(x, y, "loup")[0], 0, 0]
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    if self.__monde.plus_proche(x + i, y + j, "loup")[0] < distance_min[0]:
                        distance_min[0] = self.__monde.plus_proche(x + i, y + j, "loup")[0]
                        distance_min[1], distance_min[2] =  i, j
            self.__positionx = (self.__positionx + distance_min[1]) % self.__monde.get_infos()["dimension"]
            self.__positiony = (self.__positiony + distance_min[2]) % self.__monde.get_infos()["dimension"]
        
        self.__monde.placer_etre(self) # On replace le chasseur avec sa nouvelle position

class Tondeur:
    # Cette classe représente ce qu'est un tondeur, et la façon dont il va pouvoir vivre et évoluer dans l'écosystème

    def __init__(self, x, y, monde_du_tondeur):
        '''
        On définit l'objet tondeur, il aura comme attributs :
        - un nom (chr),
        - une position x et y sur la carte (int et int),
        - un capital qui représente le nombre de tontures qu'il a réalisé (int),
        - une instance du monde sur lequel il se trouve (Monde),
        - et un sexe choisi aléatoirement (chr)
        Prec :
        - Une position x et y (int et int),
        - et une instance du monde sur lequel il va être placé (Monde)
        Postc : Un tondeur (Tondeur) avec ses différents attributs
        '''
        self.__sexe = choice(["M", "F"]) # On donne aléatoirement un sexe au tondeur
        self.__nom = noms("tondeur", str(self.__sexe)) # On donne un nom au tondeur en fonction de son sexe
        self.__positionx = x # On donne une position x
        self.__positiony = y # On donne une position y
        self.__capital = 0 # C'est dans cet attribut que l'on va stocker le nombre de tontures que le tondeur a effectué
        self.__monde = monde_du_tondeur # On crée une instance du monde dans lequel se trouve le chasseur
        monde_du_tondeur.placer_etre(self) # On place le tondeur sur la carte
    
    def __repr__(self):
        '''
        On définit une fonction __repr__ pour obtenir le nom du tondeur une fois appelée
        Postc : Le nom (str) du tondeur
        '''
        return str(self.__nom)
    
    def get_infos(self):
        '''
        La fonction va nous servir à récupérer les données du tondeur
        Prec : Un tondeur (Tondeur)
        Postc : Un dictionnaire (dict) avec toutes ses informations
        '''
        return dict(
            {
                "nom" : self.__nom,
                "position" : [self.__positionx,self.__positiony], 
                "capital" : self.__capital,
                "sexe" : self.__sexe
            }
        )

    def tondre(self):
        '''
        Cette méthode va nous permettre de faire tondre un mouton présent sur la même case que le tondeur s'il a assez de laine
        Prec : Un tondeur (Tondeur)
        Postc : Le mouton (Mouton) qui a été tondu, ou un None si il n'y en a aucun qui est tondable
        '''
        for i in self.__monde.getCoefCarte(self.__positionx,self.__positiony)["mouton"]: # On parcourt la liste de tous les moutons présents sur la case du tondeur
            if i.get_infos()["laine"] >= i.get_infos()["laine_repousse"]: # On regarde si le mouton en question a assez de laine pour être tondu
                i.laine_coupee() # On coupe la laine du mouton
                self.__capital += 1 # On incrémente le capital du tondeur
                return i # On renvoie le nom du mouton tondu
        return None # Le cas où aucun mouton n'a été tondu

    def deplacement(self):
        '''
        On crée la méthode qui va permettre de déplacer le tondeur sur la carte, 
        elle va être construite de façon à ce que si un mouton se trouve assez près du tondeur, 
        ce dernier va le sentir et se rapprocher de lui
        Prec : Le tondeur (Tondeur) avec sa position initale
        Postc : Le tondeur (Tondeur) avec sa position mise à jour
        '''
        self.__monde.retirer_etre(self) # On retire le tondeur de la carte pour mettre à jour sa position

        x, y = self.__positionx, self.__positiony # On récupère la case du tondeur
        d = self.__monde.plus_proche(x,y,"loup")[0] # On récupère la distance du mouton le plus proche
        
        # Dans la méthode plus_proche(), 
        # la liste initale avait des valeurs gigantesques qui étaient remplacées par le mouton le plus proche,
        # si ces valeurs n'ont pas été remplacées, il n'y a pas de mouton sur la carte, 
        # on déplace alors juste le tondeur aléatoirement
        # Il y a aussi le cas où l'être cherché est trop loin, on applique alors aussi un déplacement aléatoire
        if d > 999 or self.__monde.plus_proche(x, y, "mouton")[0] > self.__monde.get_infos()["dimension"]//2 + 1: 
            # On ajoute aléatoirement une valeur comprise entre -1 et 1 à sa position x 
            # La valeur finale obtenue est mise modulo à la taille du monde car le monde est torique :
            self.__positionx = (self.__positionx + randint(-1, 1)) % self.__monde.get_infos()["dimension"]
            # On fait de même avec la position y 
            self.__positiony = (self.__positiony + randint(-1, 1)) % self.__monde.get_infos()["dimension"]
        else : # On va, par force brute, trouver la direction prise qui raproche le plus le tondeur du mouton
            distance_min = [self.__monde.plus_proche(x, y, "loup")[0], 0, 0]
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    if self.__monde.plus_proche(x + i, y + j, "loup")[0] < distance_min[0]:
                        distance_min[0] = self.__monde.plus_proche(x + i, y + j, "loup")[0]
                        distance_min[1], distance_min[2] =  i, j
            self.__positionx = (self.__positionx + distance_min[1]) % self.__monde.get_infos()["dimension"]
            self.__positiony = (self.__positiony + distance_min[2]) % self.__monde.get_infos()["dimension"]
        
        self.__monde.placer_etre(self) # On replace le tondeur avec sa nouvelle position

class Simulation :
    # Cette classe va gérer la simulation

    def __init__(self, fin, monde, dico_mouton, dico_loup, dico_chasseur, nb_tondeurs):
        '''
        On définit l'objet Simulation, il aura comme attributs 
        - un dictionnaire (dict) avec les informations sur les moutons, il contiendra :
            - le nombre de moutons (int)
            - le gain de nourriture (int)
            - la durée de repousse de la laine (int)
            - le taux de reproduction (int)
        - un dictionnaire (dict) avec les informations sur les loups, il contiendra :
            - le nombre de loups (int)
            - le gain de nourriture (int)
            - la probabilité de manger ou non un mouton présent sur la case du loup (int)
            - le taux de reproduction (int)
        - un dictionnaire (dict) avec les informations sur les chasseurs, il contiendra :
            - le nombre de chasseurs (int)
            - le taux de précison des chasseurs (int) 
        - le nombre de tondeurs voulus sur la carte (int)
        - une horloge (int) (initée à 0), 
        - une liste des moutons (list),
        - une liste des loups (list) 
        - une valeur de temps (int) qui définira la fin du monde, 
        - une instance de l'objet monde (Monde), 
        - le nombre de carré d'herbe (list) trouvé au fur et à mesure de la simulation, idem pour les moutons
        Prec : 
        - des dictionnaires (dict) qui vont contenir toutes les données voulues à propos des moutons (Mouton), des loups (Loup), et des chasseurs (Chasseur)
        - un nombre (int) de tondeurs voulus sur la carte
        - une valeur pour laquelle l'horloge devra s'arrêter (int), 
        - une instance du monde (Monde)
        Postc : Les deux listes (list) de résultats
        '''
        self.__infos_moutons = dico_mouton # On stocke le dictionnaire dans les attributs pour pouvoir l'utiliser dans la méthode de la simulation
        self.__infos_loups = dico_loup # On fait de même pour les loups
        self.__infos_chasseurs = dico_chasseur # Et pour les chasseurs
        self.__horloge = 0 # L'horloge initée à 0
        self.__fin_du_monde = fin # L'heure (par rapport à l'horle créée auparavant) de fin du monde, et donc fin de la simulation
        self.__moutons = [] # Cette liste va contenir tous les moutons du monde
        for i in range(dico_mouton["nb"]): # On crée des moutons
            posx = randint(0,monde.get_infos()["dimension"]-1)
            posy = randint(0,monde.get_infos()["dimension"]-1)
            mouton_aleatoire = Mouton(
                gain_n = dico_mouton["gain_n"],
                x = posx, 
                y = posy, 
                duree_laine = dico_mouton["duree_laine"], 
                taux_rep = dico_mouton["taux_rep"], 
                monde_du_mouton = monde)
            self.__moutons.append(mouton_aleatoire)
        self.__loups = [] # Cette liste va contenir tous les loups du monde
        for i in range(dico_loup["nb"]): # On crée des loups
            posx = randint(0,monde.get_infos()["dimension"]-1)
            posy = randint(0,monde.get_infos()["dimension"]-1)
            loup_aleatoire = Loup(
                gain_n = dico_loup["gain_n"], 
                x = posx, 
                y = posy, 
                taux_rep = dico_loup["taux_rep"],
                proba_manger = dico_loup["proba_manger"], 
                monde_du_loup = monde)
            self.__loups.append(loup_aleatoire)
        self.__chasseurs = [] # Cette liste va contenir tous les chasseurs du monde
        for i in range(dico_chasseur["nb"]): # On crée des chasseurs
            posx = randint(0,monde.get_infos()["dimension"]-1)
            posy = randint(0,monde.get_infos()["dimension"]-1)
            chasseur_aleatoire = Chasseur( 
                x = posx, 
                y = posy,
                taux_prec = dico_chasseur["taux_prec"],
                monde_du_chasseur = monde)
            self.__chasseurs.append(chasseur_aleatoire)
        self.__tondeurs = [] # Cette liste va contenir tous les tondeurs du monde
        for i in range(nb_tondeurs): # On crée des tondeurs
            posx = randint(0,monde.get_infos()["dimension"]-1)
            posy = randint(0,monde.get_infos()["dimension"]-1)
            tondeur_aleatoire = Tondeur( 
                x = posx, 
                y = posy,
                monde_du_tondeur = monde)
            self.__tondeurs.append(tondeur_aleatoire)
        self.__monde = monde # On crée une instance du monde dans la simulation
        self.__resulats_herbe = [] # On crée une liste vide qui va contenir la quantité d'hebre au fur à et mesure des tours
        self.__resulats_moutons = [] # On fait de même pour les moutons
        self.__resulats_loups = [] # On fait de même pour les loups
        self.__resulats_chasseurs = {} # Ce dictionnaire va stocker les scores des chasseurs
        self.__resulats_tondeurs = {} # Ce dictionnaire contenir les capitaux des tondeurs au fur et à mesure

    def sim(self):
        ''' On crée la méthode qui va gérer la simulation. '''

        def incrHorloge():
            ''' Cette fonction va nous servir à augmenter l'horloge de 1 à chaque appel. '''
            self.__horloge += 1

        def herbePousse():
            ''' Cette fonction fait pousser l'herbe du monde. '''
            self.__monde.herbePousse() # On utilise la méthode de la classe Monde pour le faire

        def lainePousse():
            ''' Cette fonction fait pousser la laine de tous les moutons du monde '''
            for i in self.__moutons: # On parcourt tous les moutons
                i.laine_pousse() # On fait pousser la laine de chacun d'eux

        def energie():
            ''' Cette fonction va faire appel à la méthode variationEnergie de la classe Mouton pour chaque mouton du monde.
            Elle va aussi s'occuper de faire manger les loups'''
            for i in self.__moutons:
                energie = i.variationEnergie()
                if energie <= 0: # Si le mouton n'a plus du tout d'énergie, il meurt
                    if i.get_infos()["sexe"] == "M": # Message pour si cétait un mâle
                        print(
                            str(i) + " est mort de manque d'énergie..."
                            ) # On affiche un message au joueur pour annoncer la mort de l'animal
                    else : # Message pour si c'est une femelle
                        print(
                            str(i) + " est morte de manque d'énergie..."
                            )
                    self.__monde.retirer_etre(i) # On retire le mouton de la carte
                    self.__moutons.remove(i) # On retire le mouton de la liste des moutons, R.I.P.
            for i in self.__loups:
                variation = i.variationEnergie()
                if variation[0] <= 0: # Si le loup n'a plus d'énergie du tout, il meurt
                    if i.get_infos()["sexe"] == "M": # Message pour si cétait un mâle
                        print(
                            str(i) + " est mort de manque d'énergie..."
                            ) # On affiche un message au joueur pour annoncer la mort de l'animal
                    else : # Message pour si c'est une femelle
                        print(
                            str(i) + " est morte de manque d'énergie..."
                            )
                    self.__monde.retirer_etre(i) # On retire le loup de la carte
                    self.__loups.remove(i) # On retire le loup de la liste des loups
                if variation[1] != None: # S'il y a un mouton qui a été mangé :
                    if variation[1].get_infos()["sexe"] == "M": # Message pour si cétait un mâle
                        print(
                            str(variation[1]) + " s'est fait mangé par " + str(i) + " !!"
                            ) # On affiche un message au joueur pour annoncer la mort de l'animal
                    else : # Message pour si c'est une femelle
                        print(
                            str(variation[1]) + " s'est faite mangé par " + str(i) + " !!"
                            )
                    self.__monde.retirer_etre(variation[1]) # On retire le mouton mangé de la carte
                    self.__moutons.remove(variation[1]) # On retire le mouton de la liste des moutons

        def reproduction():
            ''' Cette fonction va servir à faire se reproduire les moutons puis les loups '''
            dico_mouton = self.__infos_moutons # On récupère le dictionnaire d'informations des moutons
            dico_loup = self.__infos_loups # et celui des loups
            for i in self.__moutons: # On parcourt tous les moutons
                if i.get_infos()["sexe"] == "F" and randint(1,100) <= dico_mouton["taux_rep"]: # On part du principe que seules les femelles peuvent donner naissance, et on vérifie ensuite si la probabilité passe
                    bebe = Mouton( # On crée le bébé mouton
                        dico_mouton["gain_n"], 
                        i.get_infos()["position"][0], # On récupère la position en ordonnée 
                        i.get_infos()["position"][1], # Puis en abscisse
                        dico_mouton["duree_laine"],
                        dico_mouton["taux_rep"], 
                        self.__monde
                    )
                    self.__moutons.append(bebe) # On ajoute le mouton à la liste des moutons
                    if bebe.get_infos()["sexe"] == "M": # Message d'annonce de la naissance pour un garçon
                        print( 
                            "Un nouveau né ! C'est un garçon et il s'appelle " + str(bebe) + ", et sa mère est Madame " + str(i) + " !!"
                            ) 
                    else :
                        print(
                            "Un nouveau né ! C'est une fille et elle s'appelle " + str(bebe) + ", et sa mère est Madame " + str(i) + " !!"
                        )
            for i in self.__loups: # On parcourt ensuite les loups
                if i.get_infos()["sexe"] == "F" and randint(1,100) <= dico_loup["taux_rep"]: # Même principe que pour les moutons
                    bebe = Loup(
                        dico_loup["gain_n"],
                        i.get_infos()["position"][0],
                        i.get_infos()["position"][1],
                        dico_loup["taux_rep"],
                        dico_loup["proba_manger"],
                        self.__monde
                    )
                    self.__loups.append(bebe)
                    if bebe.get_infos()["sexe"] == "M": # Message d'annonce de la naissance pour un garçon
                        print( 
                            "Un nouveau né ! C'est un garçon et il s'appelle " + str(bebe) + ", et sa mère est Madame " + str(i) + " !!"
                            ) 
                    else :
                        print(
                            "Un nouveau né ! C'est une fille et elle s'appelle " + str(bebe) + ", et sa mère est Madame " + str(i) + " !!"
                        )
        
        def tuer():
            ''' Cette fonction va servir à faire tuer les loups par les chasseurs en appelant la fonction chasser() '''
            for i in self.__chasseurs: # On parcourt tous les chasseurs
                victime = i.chasser() # On fait chasser chaque chasseur
                if victime == "Raté": # Cas où le chasseur rate son coup
                    print(
                        str(i) + " a tiré une balle qui n'a pas atteint sa cible !"
                    )
                elif victime != None: # Si une victime a été faite
                    self.__monde.retirer_etre(victime) # On retire le loup tué de la carte
                    self.__loups.remove(victime) # Mais aussi de la liste des loups de la simulation
                    # On annonce la nouvelle au joueur
                    if victime.get_infos()["sexe"] == "M": # Message dans le cas où la victime est un mâle
                        print(
                            str(victime) + " a été tué par " + str(i) + " !!" 
                        )
                    else :
                        print(
                            str(victime) + " a été tuée par " + str(i) + " !!" 
                        )
        
        def tonte():
            ''' Cette fonction va servir à faire tondre les moutons par les tondeurs '''
            for i in self.__tondeurs:
                mouton = i.tondre()
                if mouton != None: # Si un mouton a bien perdu sa laine
                    print(
                        str(mouton) + " s'est fait tondre par " + str(i)
                    )
                
        def deplacer():
            ''' Cette fonction fait se déplacer tous les êtres du monde '''
            for i in self.__moutons:
                i.deplacement()
            for i in self.__loups:
                i.deplacement()
            for i in self.__chasseurs:
                i.deplacement()
            for i in self.__tondeurs:
                i.deplacement()
        
        def compte():
            ''' Cette fonction sert à ajouter aux listes resultats_herbe et resultats_moutons la quantité d'herbe et de moutons présents sur la carte.
            Mais aussi les score des chasseurs. '''
            infos = self.__monde.compte()
            self.__resulats_herbe.append(infos["herbe"])
            self.__resulats_moutons.append(infos["mouton"])
            self.__resulats_loups.append(infos["loup"])
            if infos["mouton"] >= self.__infos_moutons["moutons_max"]: # Si le nombre maximal de moutons fixé est atteint la simulation s'arrête
                self.__horloge = self.__fin_du_monde
        
        while not self.__horloge == self.__fin_du_monde: # Boucle d'exécution des fonctions
            incrHorloge() # On incrémente l'horloge
            herbePousse() # On fait pousser l'herbe
            lainePousse() # On fait pousser la laine
            energie() # On fait varier l'énergie des êtres
            reproduction() # On fait se reproduire les êtres
            tuer() # On fait chasser les chasseurs
            tonte() # On fait tondre 
            deplacer() # On fait se déplacer les êtres
            compte() # On met à jour les listes de résultats
        
        # On met dans les dictionnaires respectifs les scores et les capitaux des différents
        for i in self.__chasseurs: # On récupère tous les scores de chasseurs
            self.__resulats_chasseurs[i] = i.get_infos()["score"]
        for i in self.__tondeurs: # On récupère tous les capitaux des tondeurs
            self.__resulats_tondeurs[i] = i.get_infos()["capital"]

        # On met fin au monde à la sortie de la boucle
        return self.__resulats_herbe, self.__resulats_moutons, self.__resulats_loups, self.__resulats_chasseurs, self.__resulats_tondeurs
