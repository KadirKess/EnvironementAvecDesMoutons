# EnvironementAvecDesMoutons

## Quoi ?? :

### Obectif premier du projet :

Le but de ce projet est de créer un environnement dans lequel nous plaçons différents êtres qui évoluent les uns en fonction des autres. Il y a donc des interractions entre ces derniers qui vont avoir des effets sur les populations de l'écosystème.
Nous essayons donc de créer une sorte de reproduction très simpliste d'un écosystème réel.

### Application sur Python :

L'utilisation de le PPO est particulièrement pertinente puisque grâce à cela nous pouvons faire intéragir les différents êtres du monde entre eux et de manière poussée.

### Comment ça marche ? :

Nous allons créer 6 classes au total :
- Une classe Monde sur lequel toutes les interractions vont avoir lieu.
- Une classe Mouton, qui va manger de l'herbe et se reproduire sur la carte du monde.
- Une classe Loup, qui mange les moutons et se reproduisent.
- Une classe Chasseur, qui est immortel et qui tue les loups, celui qui en a tué le plus aura le plus score et sera désigné vainqueur.
- Une classe Tondeur, aussi immortel, qui va tondre les moutons, celui qui aura tondu le plus aura le plus grand capital et sera désigné vainqueur
- Une classe Simulation qui va permettre de mettre en place toutes les interractions.

### Explication du code :

***La classe Monde :***
C'est la classe qui va représenter la carte sur laquelle se trouveront tous les êtres, elle est crée sous forme d'une matrice carrée. Elle va leur permettre de se déplacer ainsi que de mettre en place les interractions d'êtres se trouvants sue la même case.

Elle possède trois attributs :
- Une dimension (int), passée en paramètre, qui représente la taille de côté de la matrice.
- Une durée de repousse de l'hebre (int), qui va représenter la quantité d'herbe requise sur un carré d'herbe pour qu'il puisse être considéré comme mangeable par un mouton.
- Une carte sous forme de matrice, implémentée sous forme de listes (list) de dictionnaires (dict), le nombre de listes et de dictionnaires par liste est défini par la dimension passée en paramètre.
    - Les dictionnaires sont composés de 5 clés (herbe, mouton, loup, chasseur, tondeur), chacune amène à une liste composée des êtres présents sur la case.

Et a de nombreuses méthodes :
- **__repr__(self) :** 
Va afficher la carte.
- **get_infos(self) :** 
Va renvoyer les attributs du Monde sous forme de dictionnaire.
- **herbePousse(self) :** 
Va faire pousser l'herbe sur chaque case du monde
- **herbeMangee(self, x, y) :** 
Va être utilisée quand un mouton mange l'herbe d'une case, elle remet la quantité d'herbe de la case à 0.
- **getCoefCarte(self, x, y) :** 
Va renvoyer le dictionnaire de la case demandée, cette fonction va notamment servir pour les interractions entre êtres (exemple : on peut vérifier si un mouton se trouve sur la case d'un loup en particulier).
- **placer_etre(self, etre) :** 
Va servir à placer un être passé en paramètre dans la liste qui lui correspond. Elle va être utile durant la création des êtres ou encore dans leur déplacement.
- **retirer_etre(self, etre) :**
Va faire l'inverse de la méthode précédente, elle va être utile dans les cas de déplacement mais aussi de mort.
- **compte(self) :**
Va compter le nombre des êtres sur toute la carte sur un tour et va renvoyer le résultat sous forme d'un dictionnaire. La méthode va servir à créer les graphes de l'évolution démographique.
- **plus_proche(self, x, y, etre) :**
Va chercher l'être du type passé en paramètre le plus de proche des coordonnées, elles aussi, données en paramètre.
La méthode va renvoyer une liste sous cette forme : [distance à la case la plus proche, x de cette case, y de cette case].

***La classe Mouton :*** 
C'est la classe qui va représenter les moutons du monde, elle va les faire manger, se reproduire, mourir, ou encore se déplacer.

Elle possède 10 attributs :
- Un sexe choisit aléatoirement (chr), il va servir à de nombreuses reprises comme pour la reproduction ou encore la sélection du nom.
- Un nom définit en fonction du sexe de l'animal (str).
- Un gain de nourriture par carré d'herbe mangé (int).
- Un x (int) et un y (int) qui définissent la position du mouton sur la carte.
- Une énergie (int), qui augmente si le mouton mange et qui descend sinon.
- Une quantité de laine (int) qui définit la quantité de laine que le mouton a sur lui.
- Une durée de repousse de laine (int) qui définit la quantité de laine nécessaire pour considérer que le mouton soit "tondable".
- Un taux de reproduction (int) qui marque la probablité de reproduction.
- Une instance du monde (Monde) dans lequel se trouve le mouton.

Et elle a aussi 6 méthodes :
- **__repr__(self) :**
Va afficher le nom du mouton.
- **get_infos(self) :**
Va affichier un dictionnaire avec différents attributs du mouton. La méthode va par exemple être très utile pour déplacer le mouton sur la carte : on l'utilisera pour récupérer ses coordonnées actuelles
- **variationEnergie(self) :**
Va faire manger le mouton : s'il y a assez d'herbe sur la case du mouton pour qu'elle soit mangée, elle va être mise à 0 et on va ajouter de l'énergie au mouton, sinon on retire de de l'énergie au mouton.
La méthode renvoie l'énergie du mouton.
- **deplacement(self) :**
Va faire se déplacer le mouton sur une des 8 cases autour de lui.
- **laine_pousse(self) :**
Va faire pousser la laine du mouton.
- **laine_coupee(self) :**
Va mettre à 0 la quantité de laine que le mouton a sur lui. La méthode va être utilisée quand un tondeur tondra le mouton.

***La classe Loup :***
C'est la classe qui va représenter les loups du monde, elle va les faire chasser des moutons, se reproduire, mourir, ou encore se déplacer.

Elle possède 9 attributs :
- Un sexe choisit aléatoirement (chr).
- Un nom définit en fonction du sexe de l'animal (str).
- Un gain de nourriture par mouton mangé (int).
- Un x (int) et un y (int) qui définissent la position du loup sur la carte.
- Une énergie (int), qui augmente si le loup mange et qui descend sinon.
- Une probabilité (int) pour que le loup arrive à manger un mouton présent sur sa case, le cas où il n'y arrive pas peut être interprété comme le fait que le mouton se soit échappé.
- Un taux de reproduction (int) qui marque la probablité de reproduction.
- Une instance du monde (Monde) dans lequel se trouve le loup.

Elle a aussi 4 méthodes :
- **__repr__(self) :**
Va afficher le nom du loup.
- **get_infos(self) :**
Va renvoyer les attributs du loup sous forme de dictionnaire.
- **variationEnergie(self) :**
Va faire chasser le loup en vérifiant d'abord s'il y a un mouton sur la case de loup, puis va nourir le loup si la probabilité de réussir la chasse est vérifiée, tout en renvoyant le nom du mouton tué.
- **deplacement(self) :**
Va faire se déplacer le loup en le rapprochant du mouton le plus proche.

***La classe Chasseur :*** 
C'est la classe qui va représenter les chasseurs du monde, elle va les faire chasser des loups, et se déplacer.

Elle possède 7 attriibuts :
- Un sexe choisit aléatoirement (chr).
- Un nom définit en fonction du sexe du chasseur (str).
- Un x (int) et un y (int) qui définissent la position du chasseur sur la carte.
- Une probabilité (int) pour que le chasseur réussisse son tir.
- Un score (int) qui représente le nombre de loups tués par le chasseur.
- Une instance du monde (Monde) dans lequel se trouve le chasseur.

Elle a aussi 4 méthodes :
- **__repr__(self) :**
Va afficher le nom du chasseur.
- **get_infos(self) :**
Va renvoyer les attributs du chasseur sous forme de dictionnaire.
- **chasser(self) :**
Va faire chasser le chasseur : si un loup se trouve sur la case de ce dernier et que la probabilité de le tuer est vérifiée, alors le nom du loup tué sera renvoyé.
- **deplacement(self) :**
Va faire déplacer le chasseur en le rapprochant du loup le plus proche.

***La classe Tondeur :*** 
C'est la classe qui va représenter les tondeurs du monde, elle va les faire tondre les moutons et se déplacer.

Elle possède 6 attributs :
- Un sexe choisit aléatoirement (chr).
- Un nom définit en fonction du sexe du tondeur (str).
- Un x (int) et un y (int) qui définissent la position du tondeur sur la carte.
- Un capital (int) qui représente la quantité de laine récupérée par le tondeur.
- Une instance du monde (Monde) dans lequel se trouve le tondeur.

Elle a aussi 4 méthodes :
- **__repr__(self) :**
Va afficher le nom du tondeur.
- **get_infos(self) :**
Va renvoyer les attributs du tondeur sous forme de dictionnaire.
- **tondre(self) :**
Va tondre un mouton qui se trouve sur la case du tondeur si le mouton a assez de laine pour être tondu.
- **deplacement(self) :**
Va faire se déplacer le tondeur de façon à le rapprocher du mouton le plus proche.

***La classe Simulation :***
C'est la classe qui va servir à mettre tous les objets que nous avons créer en harmonie en les faisant interragir entre eux.

Elle possède 15 attributs :
- Un dictionnaire (dict) avec toutes les informations sur les moutons.
- Un dictionnaire (dict) avec toutes les informations sur les loups.
- Un dictionnaire (dict) avec toutes les informations sur les chasseurs.
- Une horloge (int) qui va permettre de minuter la durée de la simulation.
- Une valeur de fin du monde (int) qui définira la valeur sur laquelle l'horloge devra s'arrêter afin de mettre fin à la simulation.
- Une liste (list) qui va contenir tous les moutons du monde.
- Une liste (list) qui va contenir tous les loups du monde.
- Une liste (list) qui va contenir tous les chasseurs du monde.
- Une liste (list) qui va contenir tous les tondeurs du monde.
- Un monde (Monde) qui va représenter le monde sur lequel les êtres vont évoluer.
- Une liste (list) qui va contenir l'évolution de la quantité d'herbe au fur et à mesure de la simulation pour tracer les graphes à la fin de la simulation.
- Une liste (list) qui va contenir l'évolution de la démographie des moutons au fur et à mesure de la simulation pour tracer les graphes à la fin de la simulation.
- Une liste (list) qui va contenir l'évolution de la démograpgie des loups au fur et à mesure de la simulation pour tracer les graphes à la fin de la simulation.
- Un dictionnaire (dict) qui va contenir le score de tous les chasseurs pour pouvoir élire un gagnant à la fin de la simulation.
- Un dictionnaire (dict) qui va contenir le capital de tous les tondeurs pour pouvoir élire un gagnant à la fin de la simulation.

Elle ne possède qu'une seule et unique méthode avec des fonctions en elle qui permettent à la simulation de fonctionner.
