## Changelogs :

### V1.1 _20/03/2022_

**Améliorations :**
- Le README du projet comporte maintenant l'explication complète de l'entièreté code
- Les changelogs ont été déplacés sur le fichier Changelogs

**Corrections :**
- Quelques corrections mineures qui n'apportent pas de réel changement au code.

### V1.0 _08/03/2022_

***Tous les objectifs ont été atteints ! Nous avons donc décider d'appeler cette version la 1.0 !!***

**Améliorations :**
- Nous avons implémenter la classe Tondeur
- Nous avons ajouter un système de gagnants chez les chasseurs et les tondeurs pour ceux qui ont été les plus productifs
    - Leurs scores s'affichent en plus du graphe à la fin de la simulation

### V0.12 _06/03/2022_

**Améliorations :**
- Puisque les êtres ont maintenant des sexes, nous avons mis à jour les messages d'annonce (mort, naissance, ...), de manière à ce que les pronoms et tout le reste soient accordés en fonction du sexe de l'être
- Nous avons implémenter la classe Chasseur

**Corrections :**
- Le fait de faire se reproduire un mâle et une femelle était source de beaucoup trop de problèmes, et équilibrer l'environement relevait de l'impossible, nous avons donc décidé de changer le système de naissance : elle se fait de nouveau de manière assexuée, mais seules les femelles peuvent donner naissance
- Équilibrer la simulation de manière à ce que toutes les espèces vivent est impossible : soit les loups disparaissent en premiers, soit les moutons disparaissent et les loups meurent ensuite du manque de nourrriture, nous avons donc décider de maintenir l'équilibrage de manière à ce que le premier cas soit celui qui arrive systématiquement

### V0.11 _02/03/2022_

**Améliorations :**
- Pour une matrice de très grande taille, il fallait pré-générer énormément de noms pour les différents êtres, nous avons décider de les générer au fur et à mesure en fonction de la nécéssité
- Ajout de la classe Loup
- Il y a maintenant des sexes pour les différents êtres, c'est un argument qui rentre notamment en compte pour la reproduction
- La méthode de déplacement des animaux a été améliorée

**Corrections :**
- L'affichage du graphe est maintenant complètement fonctionel 

**À faire :**
- Dans les différentes exécutions testées, soit les loups gagnent de très loin, soit les moutons gagnent de très loin, nous avons essayer de trouver un certain équilibre, mais cela ne sera réalisable qu'à l'aide des chasseurs
    - Il faut donc créer la classe Chasseur

### V0.10 _22/02/2022 (La date d'aujourd'hui est un palindrome)_

**Améliorations :**
- Pour améliorer la lisibilité et la clarté du code, nous avons mis toutes les classes sur un seul fichier, séparément du fichier d'éxécution qui n'aura qu'à importer les différents classes
- Nous avons terminer la classe simulation et avons lancer des éxécutions pour corriger quelques bugs
- À la fin de l'éxécution de la simulation, un graphe s'affiche comme prévu pour afficher le nombre d'herbe et de moutons

**Corrections :**
- Beaucoup de petites corrections mineures de bugs tels que 
    - des valeurs qui étaient mises sous formes d'entiers au lieu d'être sous forme de variables pour ensuite être modifiées
    - des appels de méthode sur des variables qui n'étaient pas concernées
    - des listes qui n'étaient pas mises à jour après chaque éxécution du programme
    - et quelques petites coquilles
- Le tout a pu être repérer grâce aux premiers tests d'éxécution

**À faire :**
- Les valeurs d'herbe obtenues sont abhérantes, mais cela s'explique par le fait qu'il compte toutes les repousses d'herbe et non pas seulement les carrés mangeables comme un carré d'hebre
- Il faut commencer à construire la classe loup
- Le graphe n'affiche pas le nom de ce que représentent les courbes, il faut le corriger

### V0.9 _13/01/2022_ 

***Tout d'abord, Bonne Année !!***

**Améliorations :**
- Par soucis de practicité la méthode repr de la classe mouton renvoie juste le nom du mouton
- Le mouton est maintenant directement placer sur la carte durant sa définition
- La méthode de déplacement du mouton met maintenant sa position à jour sur la carte
- À la place de seulement mettre le nombre de moutons voulus initalement dans les attributs de la classe Simulation, nous avons décidé de mettre un dictionnaire avec toutes les informations de bases nécessaires durant la création d'un mouton, comme ça ces informations sont définies avant le lancement de la simulation et sont stockées de manière optimale
- Nous avons beaucoup avancé sur la méthode de simulation des Moutons

**Corrections :**
- Il restait une correction à faire sur le déplacement des moutons et elle a été faite (la carte non représentée allait de 0 à la taille de la carte au lieu de s'arrêter à la taille de la carte-1)

**À faire :** 
- Il faut continuer à construire la classe Simulation, c'est-à-dire finir la méthode de simulation des moutons
- IL faut commencer à construire la classe des loups

### V0.8 _12/12/2021_

**Améliorations :** 
- La clarté et la lisibilité du code a été améliorée, que ce soit dans les spécifications des fonctions ou encore dans la création des différents dictionnaires
- Tous les attributs de la classe Simulation ont été définis
- Une méthode placer_etre a été créée pour placer les êtres dans la liste de la case dans laquelle ils sont sur la carte
- La création de moutons aléatoires a été implémentée dans les attributs de la classe Simulation
- La création aléatoire de moutons dans les attributs de la classe Simulation est terminée

**Corrections :** 
- Nous avons fini de corriger les déplacements des moutons
- Dans le dictionnaire que compose chaque case, nous avons mit une liste de vide à la place d'un None pour un emplacement où il n'y avait rien pour prévoir le cas où deux être d'une même espèce se trouvent sur la même case
- Les méthodes de déplacement et de variation d'énergie des moutons prend maintenant en paramètre le monde dans lequel l'action est faite

**Projets :** 
- Nous sommes maintenant sûrs de vouloir implémenter une classe "tondeur", mais nous ne savons pas encore sous quelle forme les faire exister dans l'écosystème

**À faire :** 
- Il faut continuer à construire la classe Simulation

### V0.7 _06/12/2021_

**Améliorations :** 
- Nous avons décider de remplacer les entiers dans les cases de la matrice de la carte par des dictionnaires pour pouvoir y stocker directement la quantité d'herbe et les êtres sur la case.

**Corrections :** 
- Nous avons changer l'orthographe du nom des moutons.

**Projets :** 
- Nous pensons créer une classe "tondeur" qui se baladera pour tondre les moutons qu'ils croisent pour récupérer leur laine.

**À faire :** 
- Les déplacements des moutons sont bugués, il est nécessaire de corriger cela

### V0.6

C'est le premier "changelog" d'upload donc tout ce qui est déjà présent est considéré à partir du point de départ à partir de maintenant, les prochaines versions contiendront les vrais changelogs.

Pour l'instant, les classes Monde, et Mouton ont été créées avec ce qui est indiqué sur la feuille de projet, nous avons décidé de rajouter un système de noms qui seront attribués aux différents "êtres" de l'environnement, nous avons aussi rajouter de la laine aux moutons, comme l'herbre, il y a une durée de repousse avant qu'elle soit récupérable, nous ne savons pas encore comment exploiter cet ajout. Il y aura sûrement des choses à rajouter à ces deux classes.

La classe simulation a été entammée, nous travaillons encore sur ses attributs.